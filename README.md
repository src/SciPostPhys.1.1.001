# Source files for [SciPostPhys.1.1.001](https://scipost.org/SciPostPhys.1.1.001)

Source files for

_Quantum quenches to the attractive one-dimensional Bose gas: exact results_

Lorenzo Piroli, Pasquale Calabrese, Fabian H. L. Essler

Published as doi:[10.21468/SciPostPhys.1.1.001](https://doi.org/10.21468/SciPostPhys.1.1.001).


## License

&copy; L. Piroli et al.

Published by the SciPost Foundation.

Shield: [![CC BY 4.0][cc-by-shield]][cc-by]

This work is licensed under a
[Creative Commons Attribution 4.0 International License][cc-by].

[![CC BY 4.0][cc-by-image]][cc-by]

[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://i.creativecommons.org/l/by/4.0/88x31.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg
